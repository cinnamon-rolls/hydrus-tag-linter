import typing as T
import backend.hydrus
import hydrus_api
import traceback
from qtpy.QtCore import Qt, QObject
from qtpy.QtGui import QFont
from qtpy.QtWidgets import QWidget, QLabel, QSizePolicy, QMessageBox

Align = Qt.AlignmentFlag
SizePolicy = QSizePolicy.Policy
StandardButton = QMessageBox.StandardButton


def find_child(parent: QObject, child_type: type, child_name: T.Optional[str] = None):
    '''Searches by type and name first, then only by type.'''
    if child_name:
        object = parent.findChild(child_type, child_name)
        if object is not None:
            return object

    return parent.findChild(child_type)


# def confirm_dialog(parent: QWidget, text: str, informative_text: str, default_ok=True) -> bool:
#     mbox = QMessageBox(parent)
#     mbox.setText(text)
#     mbox.setInformativeText(informative_text)
#     mbox.setStandardButtons(StandardButton.Ok | StandardButton.Cancel)
#     mbox.setDefaultButton(StandardButton.Ok if default_ok else StandardButton.Cancel)
#     mbox.setEscapeButton(StandardButton.Cancel)
#     return mbox.exec() == StandardButton.Ok


def new_label(text: str, center: bool = False, word_wrap: bool = True, font_size: int = 12) -> QLabel:
    label = QLabel(text=text)
    label.setWordWrap(word_wrap)
    if center:
        label.setAlignment(Align.AlignCenter)

    font = QFont()
    font.setPointSize(font_size)
    label.setFont(font)
    return label


# wrapper that handles otherwise uncaught exceptions
def default_exceptions(func, parent: QWidget = None):
    from ui.dialogs import ErrorDialog
    def wrap_func(*args, **kwargs):
        try:
            func(*args, **kwargs)

        except backend.hydrus.HydrusConnectionError as e:
            ErrorDialog("Error Connecting", str(e)).exec()

        # while in theory hydrus_api.ConnectionError is wrapped & handled by
        # our centralized API module, in practice if the user does some
        # *really* strange things then this can leak through.
        except hydrus_api.ConnectionError as e:
            ErrorDialog("Error Connecting", str(e)).exec()

        except Exception as e:
            traceback.print_exception(e)
            message_lines = [
                "An unexpected/uncaught exception occurred. Please report this as a bug. You may continue using the app, but I recommend closing it and restarting to be safe.",
                "", "Error message:", f"{type(e).__name__}: {str(e)}"
            ]
            ErrorDialog("Error", '\n'.join(message_lines)).exec()

    return wrap_func
