import backend.hydrus
import backend.settings
import qtpy
import ui
import ui.helper
import ui.main_window
from ui.helper import default_exceptions
from ui.layouts import VBoxLayout, BoxLayout
from backend.actions import ActionContext, Action
from backend.rules import Rule
from backend.rule_slideshow import RuleSlideshow
from backend.hydrus import HydrusSearchRunner
from qtpy.QtCore import Signal
from qtpy.QtWidgets import QMessageBox, QPushButton, QWidget


class Widget(QWidget):
    def __init__(self, layout: type[BoxLayout] = VBoxLayout) -> None:
        super().__init__()
        self.setLayout(layout())

    @property
    def layout(self) -> BoxLayout:
        return super().layout()


class RuleList(Widget):
    def update_content(self, rules: list[Rule]) -> None:
        self.layout.remove_all_items()

        backend.hydrus.client().quiet_lazy_connect()
        client_disconnected = not backend.hydrus.client().connected

        if len(rules) == 0:
            self.layout.add(ui.helper.new_label("No rules are loaded.", center=True, font_size=9))
        for rule in rules:
            button_name = self._get_rule_display_name(rule)
            button = QPushButton(button_name)
            button.setSizePolicy(ui.helper.SizePolicy.Preferred, ui.helper.SizePolicy.Minimum)
            button.setDisabled(client_disconnected)
            button.clicked.connect(lambda *_, b=button, r=rule: self._on_rule_button_click(b, r))
            self.layout.add(button)

    def _get_rule_display_name(self, rule: Rule):
        parts = [rule.info.name]

        # TODO size preview temporarily(?) disabled
        #if rule.search.has_results():
        #    parts.append(f"({rule.search.result_count})")
        #else:
        #    parts.append("(?)")

        return ' '.join(parts)

    @default_exceptions
    def _on_rule_button_click(self, button: QPushButton, rule: Rule):
        search_runner = backend.hydrus.HydrusSearchRunner.create_default()

        slideshow = RuleSlideshow(rule)

        slideshow.lazy_fetch_results(search_runner)

        if slideshow.empty:
            button.setText(self._get_rule_display_name(rule))
            QMessageBox.information(None, "Empty Rule", "Rule '" + rule.info.name + "' has no results.")
            return

        ui.main_window.main_window().go_to_slideshow_page(slideshow)


class SlideshowActions(Widget):
    action_triggered = Signal(Action)

    def __init__(self) -> None:
        super().__init__()

        self._action_buttons: list[tuple[Action, QPushButton]] = []
        self._actions_layout = VBoxLayout()
        self._used_shortcuts = set()

        self.layout.add(self._actions_layout)

        self.update_content([])

    def _button_from_action(self, action: Action) -> QPushButton:
        text = action.get_name()
        shortcut = action.get_shortcut()

        if shortcut:
            text += f" ({shortcut})"

        button = QPushButton(text)

        if shortcut and not shortcut in self._used_shortcuts:
            self._used_shortcuts.add(shortcut)
            button.setShortcut(shortcut)

        button.clicked.connect(lambda *_, a=action: self.action_triggered.emit(a))

        return button

    def update_content(self, actions: list[Action]) -> None:
        self._action_buttons = []
        self._actions_layout.remove_all_items()
        self._actions_layout.add(ui.helper.new_label("Actions", center=True))
        self._used_shortcuts = set()

        if len(actions) > 0:
            for action in actions:
                button = self._button_from_action(action)
                self._action_buttons.append((action, button))
                self._actions_layout.add(button)

            self._actions_layout.addSpacing(20)

    def update_buttons(self, ctx: ActionContext):
        for action, button in self._action_buttons:
            button.setHidden(action.is_hidden(ctx))


class RuleDesc(Widget):
    def __init__(self) -> None:
        super().__init__()

    def update_content(self, rule: Rule) -> None:
        self.layout.remove_all_items()
        if not rule:
            return

        self.layout.add(ui.helper.new_label("Rule: " + rule.info.name))
        desc = ui.helper.new_label(rule.desc, font_size=9)
        self.layout.add(desc)


class TagListWidgetItem(qtpy.QtWidgets.QListWidgetItem):
    def __init__(self, tag: str) -> None:
        super().__init__()
        self.setText(tag)
        self.set_color(backend.hydrus.client().get_tag_color(tag))
        self._tag = tag

    def set_color(self, hex_color: str):
        self.setForeground(qtpy.QtGui.QBrush(qtpy.QtGui.QColor(hex_color)))

    def compare(self, other: "TagListWidgetItem"):
        return backend.hydrus.compare_tags(self._tag, other._tag)

    def __eq__(self, other):
        return self.compare(other) == 0

    def __ge__(self, other):
        return self.compare(other) >= 0

    def __le__(self, other):
        return self.compare(other) <= 0

    def __gt__(self, other):
        return self.compare(other) > 0

    def __lt__(self, other):
        return self.compare(other) < 0
