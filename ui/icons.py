from qtpy.QtGui import QIcon

# We're using famfamfam's Silk icon pack: http://www.famfamfam.com/lab/icons/silk/


class Icons:

    # The icon size in pixels.
    P_ICON_SIZE = 16

    # Names corresponding to the icon file name.
    ACCEPT: QIcon
    ADD: QIcon
    ARROW_DOWN: QIcon
    ARROW_LEFT: QIcon
    ARROW_REFRESH: QIcon
    ARROW_RIGHT: QIcon
    ARROW_UP: QIcon
    ASTERISK_YELLOW: QIcon
    BIN_CLOSED: QIcon
    BIN_EMPTY: QIcon
    BIN: QIcon
    BUG: QIcon
    CANCEL: QIcon
    COG: QIcon
    COMMENT: QIcon
    CONNECT: QIcon
    CROSS: QIcon
    DELETE: QIcon
    DISCONNECT: QIcon
    DISK: QIcon
    EMAIL: QIcon
    ERROR: QIcon
    EXCLAMATION: QIcon
    HELP: QIcon
    HOUSE: QIcon
    IMAGE: QIcon
    INFORMATION: QIcon
    KEY: QIcon
    LINK: QIcon
    PAGE_CODE: QIcon
    PASTE_PLAIN: QIcon
    RESULTSET_FIRST: QIcon
    RESULTSET_LAST: QIcon
    RESULTSET_NEXT: QIcon
    RESULTSET_PREVIOUS: QIcon
    SCRIPT: QIcon
    SOUND_MUTE: QIcon
    SOUND: QIcon
    STOP: QIcon
    TAG_BLUE_ADD: QIcon
    TAG_BLUE_DELETE: QIcon
    TAG_BLUE_EDIT: QIcon
    TAG_BLUE: QIcon
    TAG_GREEN: QIcon
    TAG_ORANGE: QIcon
    TAG_PINK: QIcon
    TAG_PURPLE: QIcon
    TAG_RED: QIcon
    TAG_YELLOW: QIcon
    TICK: QIcon

    # Aliases
    ASTERISK: QIcon
    HOME: QIcon
    INBOX: QIcon
    PASTE: QIcon
    QUESTION: QIcon
    REFRESH: QIcon
    RESULT_FIRST: QIcon
    RESULT_LAST: QIcon
    RESULT_NEXT: QIcon
    RESULT_PREV: QIcon
    RULE: QIcon
    SAVE: QIcon
    SETTINGS: QIcon
    TRASH: QIcon


# Creating a QIcon requires the QApplication to be running, so we have to
# workaround it by not creating them on import, and calling a function instead.
def load_icons():
    Icons.ACCEPT                = QIcon("ui/icons/accept.png")
    Icons.ADD                   = QIcon("ui/icons/add.png")
    Icons.ARROW_DOWN            = QIcon("ui/icons/arrow_down.png")
    Icons.ARROW_LEFT            = QIcon("ui/icons/arrow_left.png")
    Icons.ARROW_REFRESH         = QIcon("ui/icons/arrow_refresh.png")
    Icons.ARROW_RIGHT           = QIcon("ui/icons/arrow_right.png")
    Icons.ARROW_UP              = QIcon("ui/icons/arrow_up.png")
    Icons.ASTERISK_YELLOW       = QIcon("ui/icons/asterisk_yellow.png")
    Icons.BIN_CLOSED            = QIcon("ui/icons/bin_closed.png")
    Icons.BIN_EMPTY             = QIcon("ui/icons/bin_empty.png")
    Icons.BIN                   = QIcon("ui/icons/bin.png")
    Icons.BUG                   = QIcon("ui/icons/bug.png")
    Icons.CANCEL                = QIcon("ui/icons/cancel.png")
    Icons.COG                   = QIcon("ui/icons/cog.png")
    Icons.COMMENT               = QIcon("ui/icons/comment.png")
    Icons.CONNECT               = QIcon("ui/icons/connect.png")
    Icons.CROSS                 = QIcon("ui/icons/cross.png")
    Icons.DELETE                = QIcon("ui/icons/delete.png")
    Icons.DISCONNECT            = QIcon("ui/icons/disconnect.png")
    Icons.DISK                  = QIcon("ui/icons/disk.png")
    Icons.EMAIL                 = QIcon("ui/icons/email.png")
    Icons.ERROR                 = QIcon("ui/icons/error.png")
    Icons.EXCLAMATION           = QIcon("ui/icons/exclamation.png")
    Icons.HELP                  = QIcon("ui/icons/help.png")
    Icons.HOUSE                 = QIcon("ui/icons/house.png")
    Icons.IMAGE                 = QIcon("ui/icons/image.png")
    Icons.INFORMATION           = QIcon("ui/icons/information.png")
    Icons.KEY                   = QIcon("ui/icons/key.png")
    Icons.LINK                  = QIcon("ui/icons/link.png")
    Icons.PAGE_CODE             = QIcon("ui/icons/page_code.png")
    Icons.PASTE_PLAIN           = QIcon("ui/icons/paste_plain.png")
    Icons.RESULTSET_FIRST       = QIcon("ui/icons/resultset_first.png")
    Icons.RESULTSET_LAST        = QIcon("ui/icons/resultset_last.png")
    Icons.RESULTSET_NEXT        = QIcon("ui/icons/resultset_next.png")
    Icons.RESULTSET_PREVIOUS    = QIcon("ui/icons/resultset_previous.png")
    Icons.SCRIPT                = QIcon("ui/icons/script.png")
    Icons.SOUND_MUTE            = QIcon("ui/icons/sound_mute.png")
    Icons.SOUND                 = QIcon("ui/icons/sound.png")
    Icons.STOP                  = QIcon("ui/icons/stop.png")
    Icons.TAG_BLUE_ADD          = QIcon("ui/icons/tag_blue_add.png")
    Icons.TAG_BLUE_DELETE       = QIcon("ui/icons/tag_blue_delete.png")
    Icons.TAG_BLUE_EDIT         = QIcon("ui/icons/tag_blue_edit.png")
    Icons.TAG_BLUE              = QIcon("ui/icons/tag_blue.png")
    Icons.TAG_GREEN             = QIcon("ui/icons/tag_green.png")
    Icons.TAG_ORANGE            = QIcon("ui/icons/tag_orange.png")
    Icons.TAG_PINK              = QIcon("ui/icons/tag_pink.png")
    Icons.TAG_PURPLE            = QIcon("ui/icons/tag_purple.png")
    Icons.TAG_RED               = QIcon("ui/icons/tag_red.png")
    Icons.TAG_YELLOW            = QIcon("ui/icons/tag_yellow.png")
    Icons.TICK                  = QIcon("ui/icons/tick.png")

    # Aliases
    Icons.ASTERISK      = Icons.ASTERISK_YELLOW
    Icons.HOME          = Icons.HOUSE
    Icons.INBOX         = Icons.EMAIL
    Icons.PASTE         = Icons.PASTE_PLAIN
    Icons.QUESTION      = Icons.COMMENT
    Icons.REFRESH       = Icons.ARROW_REFRESH
    Icons.RESULT_FIRST  = Icons.RESULTSET_FIRST
    Icons.RESULT_LAST   = Icons.RESULTSET_LAST
    Icons.RESULT_NEXT   = Icons.RESULTSET_NEXT
    Icons.RESULT_PREV   = Icons.RESULTSET_PREVIOUS
    Icons.RULE          = Icons.PAGE_CODE
    Icons.SAVE          = Icons.DISK
    Icons.SETTINGS      = Icons.COG
    Icons.TRASH         = Icons.BIN_CLOSED
