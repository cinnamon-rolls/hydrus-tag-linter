import backend.hydrus
import backend.settings
import typing as T
import ui.dialogs
from ui.dialogs.tag_services_dialog import TagServicesDialog
import ui.helper
import ui.widgets
from .page import Page
from qtpy.QtWidgets import QMenu, QAction, QInputDialog, QLineEdit
from ui.dialogs import InformationDialog, ErrorDialog, WarningDialog
from ui.helper import default_exceptions
from ui.icons import Icons


class HomePage(Page):
    _ui_file = "ui/pages/templates/home.ui"

    def __init__(self) -> None:
        super().__init__()

        self.rule_list = ui.widgets.RuleList()
        item = self.layout.replaceWidget(self.widget.rule_list, self.rule_list)
        item.widget().deleteLater()

        self.menu_rules = QMenu("Rules")
        self.menu_rules.addAction("Reload", self._reload_rules)

        self.menu_hydrus = QMenu("Hydrus")
        self.menu_hydrus.addAction("Set up connection", lambda: ui.dialogs.HydrusConnectDialog().exec()).setShortcut("f9")
        self.menu_hydrus.addAction("Retry connection", self._on_retry_connection)
        self.menu_hydrus.addAction("Configure tag services", self._show_tag_service_changer)
        self.menu_hydrus.addAction("About", self._show_about_hydrus)

        # I'm not sure if there should be an "Extras" menu but I'm not sure where to put this
        self.menu_extras = QMenu("Extras")
        self.menu_extras.addAction("Open slideshow with all files", self._open_everything_slideshow)
        self.menu_extras.addAction("Paste a rule...", self._open_rule_paste)
        self.menu_extras.addAction("Open archive/delete filter", self._open_archive_delete_filter)

        self.cached_rules = None

    def get_menus(self) -> list[T.Union[QMenu, QAction]]:
        return [self.menu_rules, self.menu_hydrus, self.menu_extras]

    def load(self) -> None:
        if self.cached_rules is None:
            self._reload_rule_cache()
        self.rule_list.update_content(self.cached_rules)

    @default_exceptions
    def _show_about_hydrus(self):
        ui.dialogs.AboutDialog("About Hydrus", str(backend.hydrus.client().get_versioning()), True).exec()

    def _reload_rules(self) -> None:
        self._reload_rule_cache()
        self.rule_list.update_content(self.cached_rules)

    def _reload_rule_cache(self) -> None:
        rule_file_paths = backend.settings.get_settings().rule_file_paths
        cached_rules = backend.rules.load_rules_in_dirs(rule_file_paths)
        self.cached_rules = cached_rules

    @default_exceptions
    def _show_tag_service_changer(self) -> None:
        TagServicesDialog().exec()

    @default_exceptions
    def _open_everything_slideshow(self) -> None:
        import backend.query
        from backend.query_slideshow import QuerySlideshow

        query = backend.query.EverythingQuery()

        search_runner = backend.hydrus.HydrusSearchRunner.create_default()
        slideshow = QuerySlideshow(query)
        slideshow.lazy_fetch_results(search_runner)

        ui.main_window.main_window().go_to_slideshow_page(slideshow)

    @default_exceptions
    def _open_rule_paste(self):
        import backend.rules
        from backend.rule_slideshow import RuleSlideshow

        text, ok = QInputDialog.getMultiLineText(
            self,
            "Paste Rule JSON",
            "Paste the JSON data of the rule you'd like to try out",
            "")

        if not ok: return

        text = text.strip()
        if text == "": return

        try:
            temp_rule = backend.rules.create_rule_from_json_str(text)
        except backend.rules.RuleCreationError as e:
            ErrorDialog("Error loading temporary rule", f"Failed to interpret as rule: {str(e)}").exec()
            return

        search_runner = backend.hydrus.HydrusSearchRunner.create_default()
        slideshow = RuleSlideshow(temp_rule)
        slideshow.lazy_fetch_results(search_runner)

        if slideshow.empty:
            InformationDialog("Empty Rule", f'Rule "{temp_rule.info.name}" has no results').exec()
            return

        ui.main_window.main_window().go_to_slideshow_page(slideshow)

    @default_exceptions
    def _open_archive_delete_filter(self):
        import backend.query
        from backend.archive_delete_slideshow import ArchiveDeleteSlideshow
        slideshow = ArchiveDeleteSlideshow()
        slideshow.lazy_fetch_results(backend.hydrus.HydrusSearchRunner.create_default())
        ui.main_window.main_window().go_to_slideshow_page(slideshow)

    def _on_retry_connection(self):
        try:
            backend.hydrus.client().connect()
        except backend.hydrus.HydrusConnectionError as e:
            WarningDialog("Connection Failed", str(e)).exec()
        else:
            InformationDialog("Connection Success", "Connected to Hydrus API succesfully.").exec()
            self.load()
