from .page import Page

from .home_page import HomePage
from .settings_page import SettingsPage
from .slideshow_page import SlideshowPage
