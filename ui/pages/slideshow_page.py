import backend.hydrus
import backend.settings
import qtpy
import qtpy.QtCore
import typing as T
import ui
import ui.clipboard
import ui.helper
import ui.main_window
import ui.widgets
from .page import Page
from backend.actions import Action, ActionContext
from backend.empty_slideshow import EmptySlideshow
from backend.hydrus import HydrusSearchRunner, HydrusClient, HydrusConnectionError
from backend.settings import Settings
from backend.slideshow import Slideshow
from qtpy.QtWebEngineWidgets import QWebEngineView
from qtpy.QtWidgets import QMenu, QAction
from ui.dialogs import ErrorDialog, InformationDialog
from ui.helper import default_exceptions


class SlideshowPage(Page):
    _ui_file = "ui/pages/templates/slideshow.ui"

    HTML_PRESET = '''
<html>
    <head>
        <style>
            body {
                margin: 0;
            }
            div#wrapper {
                width: 100vw;
                height: 100%;
                text-align: center;
                background-color: transparent;
            }
            div#wrapper img, div#wrapper video {
                display: block;
                position: absolute;
                margin: auto;
                inset: 0;
                max-width: 100%;
                max-height: 100%;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            REPLACEME
        </div>
    </body>
</html>
'''

    def __init__(self) -> None:
        super().__init__()
        self._slideshow = None
        self.current_idx = 0

        self.rule_actions = ui.widgets.SlideshowActions()
        self.rule_actions.action_triggered.connect(self.run_action)

        self.set_slideshow(EmptySlideshow())

        # Redundant, left them commented out just in case.
        # self._add_shortcut_action("right", self._on_right)
        # self._add_shortcut_action("left" , self._on_left)
        # self._add_shortcut_action("home" , self._on_home)
        # self._add_shortcut_action("end"  , self._on_end)
        self._add_shortcut_action("esc"  , self._on_esc)

        self.menu_slideshow = QMenu("Slideshow")
        self.menu_slideshow.addAction("Next", self._on_right).setShortcut("right")
        self.menu_slideshow.addAction("Previous", self._on_left).setShortcut("left")
        self.menu_slideshow.addAction("Jump to first", self._on_home).setShortcut("home")
        self.menu_slideshow.addAction("Jump to last", self._on_end).setShortcut("end")
        self.menu_slideshow.addSeparator()
        self.menu_slideshow.addAction("Re-fetch contents", self._on_refetch_contents)
        self.menu_slideshow.addSeparator()
        self.menu_slideshow.addAction("Copy first 100 hashes", lambda: self._on_copy_hashes(limit=100))
        self.menu_slideshow.addAction("Copy all hashes (slow!)", self._on_copy_all_hashes)
        self.menu_slideshow.addSeparator()
        self.menu_slideshow.addAction("Debug info", self._show_slideshow_info)

        self.menu_file = QMenu("File")
        self.menu_file.addAction("Add tag(s)", self._on_file_add_tags)
        self.menu_file.addAction("Remove tag(s)", self._on_file_remove_tags)
        self.menu_file.addSeparator()
        self.menu_file.addAction("Move to archive", self._on_file_move_to_archive)
        self.menu_file.addAction("Move to inbox", self._on_file_move_to_inbox)
        self.menu_file.addAction("Move to trash", self._on_file_move_to_trash)
        self.menu_file.addAction("Move out of trash", self._on_file_move_out_of_trash)
        self.menu_file.addSeparator()
        self.menu_file.addAction("Copy hash", self._on_file_copy_hash)
        self.menu_file.addSeparator()
        self.menu_file.addAction("Debug info", self._show_file_debug_info)

        self.build_webview()

        item = self.widget.left_panel.layout().replaceWidget(self.widget.rule_actions, self.rule_actions)
        item.widget().deleteLater()

    # Using these _get_X functions prevents a dependency on singleton behavior
    # from spreading throughout the code. If the method of obtaining an
    # instance of these objects changes, then its isolated to here.

    def get_menus(self) -> list[T.Union[QMenu, QAction]]:
        return [self.menu_slideshow, self.menu_file]

    def _get_search_runner(self) -> HydrusSearchRunner:
        return HydrusSearchRunner.create_default()

    def _get_client(self) -> HydrusClient:
        return backend.hydrus.client()

    def _get_settings(self) -> Settings:
        return backend.settings.get_settings()

    def _get_tag_service_name(self):
        return self._get_settings().main_tag_service

    def _get_action_context(self):
        tag_service = self._get_tag_service_name()
        return ActionContext(client=self._get_client(), slideshow=self.slideshow, tag_service_name=tag_service)

    def set_slideshow(self, slideshow : Slideshow):
        if not isinstance(slideshow, Slideshow):
            raise TypeError(f"Expected object of type Slideshow, got instead {type(slideshow).__name__}")
        if not slideshow.are_results_fetched:
            search_runner = HydrusSearchRunner.create_default()
            slideshow.fetch_results(search_runner)
        self._slideshow = slideshow

    @property
    def slideshow(self):
        return self._slideshow

    def build_webview(self) -> None:
        wv = QWebEngineView()
        wv.setContent(b"")
        self.web_page = wv.page()
        self.web_page.setBackgroundColor(qtpy.QtCore.Qt.transparent)
        #TODO: connect back/forward actions of the page to methods that update the index and values accordingly.

        # Ignore this, it's quick workaround to help with a thing I've been trying out in Qt Designer. - laure
        wv.setSizePolicy(self.widget.web_view.sizePolicy())

        item = self.widget.layout().replaceWidget(self.widget.web_view, wv)
        item.widget().deleteLater()
        self.widget.web_view = wv

    @default_exceptions
    def run_action(self, action: Action) -> None:
        current_position = self.slideshow.position
        current_file = self.slideshow.get_current_file(self._get_client())

        if current_file is None:
            return

        ctx = self._get_action_context()

        if not action.is_enabled(ctx):
            return

        result = action.run(ctx)

        if result.message is not None:
            if result.success:
                title = "Note"
            else:
                title = "Action failed"
            ErrorDialog(title, str(result.message)).exec()

        # the result has politely suggested that the user most likely wants to move to the next file
        # consider adding a configuration for whether we respect the suggestion
        # (Skip and Unskip are 'rude' - they move to the next file by themselves)
        if result.should_advance:
            self.slideshow.move_to_next_file()

        # after the action has run, we cannot assume our snapshot of the metadata is valid anymore
        self._invalidate_current_file_metadata()

        # current file has changed
        if current_position != self.slideshow.position:
            self._load_current_file()

    @default_exceptions
    def _load_file_view(self, file: backend.hydrus.HydrusFile) -> None:
        mime = file.mime
        url = file.full_file_url

        if mime.startswith("image/"):
            self.web_page.setHtml(self.HTML_PRESET.replace("REPLACEME", f"<img src='{url}'>"))
        elif mime.startswith("video/"):
            #TODO: Make muted, and potentially others, configurable.
            self.web_page.setHtml(
                self.HTML_PRESET.replace("REPLACEME", f"<video autoplay loop controls muted src='{url}'>"))
        else:
            self.web_page.setHtml(
                self.HTML_PRESET.replace("REPLACEME", "The file could not be loaded (mimetype is not image or video)."))

    @default_exceptions
    def _load_tag_list(self, file: backend.hydrus.HydrusFile) -> None:
        #TODO: get tags from a configured list of services, not just the one the linter modifies. See issue #4
        settings = self._get_settings()
        services = [settings.main_tag_service] + settings.additional_tag_services
        # TODO: configure the statuses to check
        statuses = [backend.hydrus.TAG_STATUS_CURRENT]
        tags = file.get_tags(services, statuses)
        self.widget.tag_list.clear()
        for tag_str in tags:
            tag_item = ui.widgets.TagListWidgetItem(tag_str)
            self.widget.tag_list.addItem(tag_item)

    @default_exceptions
    def _load_current_file(self) -> None:
        file = self.slideshow.get_current_file(self._get_client())
        if file is None:
            return
        self.rule_actions.update_buttons(self._get_action_context())

        self._load_file_view(file)
        self._load_tag_list(file)

    def _invalidate_current_file_metadata(self) -> None:
        if self.slideshow.empty:
            return
        self.slideshow.invalidate_current_file_metadata()
        file = self.slideshow.get_current_file(self._get_client())
        if file is None:
            return
        self.rule_actions.update_buttons(self._get_action_context())
        self._load_tag_list(file)

    def unload(self) -> None:
        self.current_idx = 0
        # We have to replace the entire webview every time to avoid a flicker with the last rendered frame.
        # This could be avoided if we find a way to force that frame clearing when leaving the page.
        self.build_webview()

    def load(self) -> None:
        self.rule_actions.update_content(self.slideshow.get_actions())
        self.widget.rule_name.setText(self.slideshow.get_title())
        self.widget.rule_desc.setText(self.slideshow.get_description())
        self._load_current_file()

    def _show_slideshow_info(self):
        info_lines = [f"Position: {self.slideshow.position}", f"Size: {self.slideshow.size}"]
        message = "\n".join(info_lines)
        InformationDialog("Info", message).exec()

    def _on_esc(self):
        ui.main_window.main_window().go_home()

    def _on_left(self):
        self.slideshow.move_to_previous_file()
        self.load()

    def _on_right(self):
        self.slideshow.move_to_next_file()
        self.load()

    def _on_home(self):
        self.slideshow.move_to_first_file()
        self.load()

    def _on_end(self):
        self.slideshow.move_to_last_file()
        self.load()

    def _on_refetch_contents(self):
        self.slideshow.fetch_results(self._get_search_runner())
        self.load()

    def _on_copy_all_hashes(self):
        ui.clipboard.copy_text('\n'.join(self.slideshow.get_hashes(self._get_client())))

    def _on_copy_hashes(self, limit=100):
        ui.clipboard.copy_text('\n'.join(self.slideshow.get_hashes(self._get_client(), limit=limit)))

    # Quick file actions - done via the toolbar

    @default_exceptions
    def _maybe_do_quick_file_action(self, func):
        if self.slideshow.empty:
            ErrorDialog(
                "No file present",
                "Your requested action could not be performed as the slideshow is not currently viewing a file").exec()
            return

        file = self.slideshow.get_current_file(self._get_client())

        try:
            result = func(file)
        except HydrusConnectionError as e:
            result = (False, f"Connection to client failed: {str(e)}")

        if result is None:
            result = (True, None)

        success, message = result

        if message is None:
            if not success:
                InformationDialog("Failure", "Action failed (unspecified message)").exec()
        else:
            if success:
                InformationDialog("Info (Success)", message).exec()
            else:
                InformationDialog("Failure", message).exec()

        self._invalidate_current_file_metadata()

    def _on_file_move_to_inbox(self):
        def func(file):
            self._get_client().unarchive_files([file.hash])
        self._maybe_do_quick_file_action(func)

    def _on_file_move_to_archive(self):
        def func(file):
            self._get_client().archive_files([file.hash])
        self._maybe_do_quick_file_action(func)

    def _on_file_move_to_trash(self):
        def func(file):
            self._get_client().delete_files([file.hash])
        self._maybe_do_quick_file_action(func)

    def _on_file_move_out_of_trash(self):
        def func(file):
            self._get_client().undelete_files([file.hash])
        self._maybe_do_quick_file_action(func)

    # TODO laure, this is duplicated in backend.actions.TagPromptAction
    # please consider centralizing this
    def _show_tag_prompt(self, is_action_add : bool):
        from qtpy.QtWidgets import QInputDialog, QLineEdit
        title = "Apply Tags" if is_action_add else "Remove Tags"
        text, ok = QInputDialog.getText(None, title, f"Provide a list of comma-separated tags", QLineEdit.Normal)
        if not ok:
            return None
        return [x.strip() for x in text.split(",")]

    def _on_file_add_tags(self):
        from backend.hydrus import TAG_ACTION_ADD
        def func(file):
            tags = self._show_tag_prompt(True)
            if tags is None or len(tags) == 0: return

            att = {}
            att[TAG_ACTION_ADD] = tags
            sntatt = {}
            sntatt[self._get_tag_service_name()] = att

            self._get_client().change_tags(hashes=[file.hash], service_names_to_actions_to_tags=sntatt)

        self._maybe_do_quick_file_action(func)

    def _on_file_remove_tags(self):
        from backend.hydrus import TAG_ACTION_DELETE
        def func(file):
            tags = self._show_tag_prompt(False)
            if tags is None or len(tags) == 0: return

            att = {}
            att[TAG_ACTION_DELETE] = tags
            sntatt = {}
            sntatt[self._get_tag_service_name()] = att

            self._get_client().change_tags(hashes=[file.hash], service_names_to_actions_to_tags=sntatt)

        self._maybe_do_quick_file_action(func)

    def _show_file_debug_info(self):
        def func(file):
            lines = [
                f"ID: {file.file_id}",
                f"Hash: {file.hash}",
                f"Size: {file.size}",
                f"Archive? {file.is_in_archive}",
                f"Trash? {file.is_in_trash}",
            ]
            InformationDialog("File Info", '\n'.join(lines)).exec()

        self._maybe_do_quick_file_action(func)

    def _on_file_copy_hash(self):
        def func(file):
            ui.clipboard.copy_text(file.hash)
        self._maybe_do_quick_file_action(func)
