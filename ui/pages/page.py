import typing as T
import ui
from qtpy.QtWidgets import QWidget, QMenu, QAction, QBoxLayout


# TODO: Replace Page with this.
class Page(ui.UIObject):
    _MINIMUM_SIZE = (640, 480)

    def __new__(cls: type["Page"]) -> "Page":
        page = super().__new__(cls)
        page._widget = ui.loadUi(cls._ui_file, QWidget, QBoxLayout)
        page._widget.setMinimumSize(*cls._MINIMUM_SIZE)

        return page

    def __init__(self) -> None:
        self._add_shortcut_action("f5", self.load)

    def addLayout(self) -> None:
        pass

    def addWidget(self) -> None:
        pass

    def _add_shortcut_action(self, shortcut: str, trigger_func: T.Callable) -> None:
        the_action = QAction(shortcut, self.widget)
        the_action.setShortcut(shortcut)
        the_action.triggered.connect(trigger_func)
        self.widget.addAction(the_action)

    def unload(self) -> None:
        pass

    def load(self) -> None:
        pass

    def get_menus(self) -> list[T.Union[QMenu, QAction]]:
        return []

    @property
    def widget(self) -> QWidget:
        return self._widget

    @property
    def layout(self) -> QBoxLayout:
        return self.widget.layout()

# class Page(QWidget):
#     def __init__(self, ui_file_path: str) -> None:
#         super().__init__()
#         self.setLayout(HBoxLayout())
#         self.setMinimumSize(640, 480)

#         self._ui = qtpy.uic.loadUi(ui_file_path)

#         self.layout.add(self.ui)

#         self._add_shortcut_action("f5", self.load)

#     @property
#     def ui(self) -> QWidget:
#         return self._ui

#     @property
#     def layout(self) -> BoxLayout:
#         return super().layout()

#     def unload(self) -> None:
#         pass

#     def load(self) -> None:
#         pass

#     def get_menus(self) -> list[T.Union[QMenu, QAction]]:
#         return []

#     def _add_shortcut_action(self, shortcut_str, trigger_func):
#         the_action = QAction(shortcut_str, self)
#         the_action.setShortcut(shortcut_str)
#         the_action.triggered.connect(trigger_func)
#         self.addAction(the_action)
