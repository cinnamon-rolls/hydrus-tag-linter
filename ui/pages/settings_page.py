from .page import Page


class SettingsPage(Page):
    _ui_file = "ui/pages/templates/settings.ui"
