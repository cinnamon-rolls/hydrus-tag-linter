import typing as T
from qtpy.QtWidgets import QBoxLayout, QWidget, QLayout


class BoxLayout(QBoxLayout):
    def __init__(self, direction: QBoxLayout.Direction) -> None:
        super().__init__(direction)
        self.setContentsMargins(0, 0, 0, 0)

    def remove_all_items(self):
        for i in reversed(range(self.count())):
            item = self.itemAt(i)
            widget = item.widget()
            layout = item.layout()
            if widget:
                widget.deleteLater()
            if layout:
                layout.deleteLater()
            self.removeItem(item)

    def add(self, item: T.Union[QWidget, QLayout], stretch: int = 0) -> None:
        if isinstance(item, QLayout):
            self.addLayout(item, stretch=stretch)
        else:
            self.addWidget(item, stretch=stretch)


class VBoxLayout(BoxLayout):
    def __init__(self) -> None:
        super().__init__(QBoxLayout.Direction.TopToBottom)


class HBoxLayout(BoxLayout):
    def __init__(self) -> None:
        super().__init__(QBoxLayout.Direction.LeftToRight)
