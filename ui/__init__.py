from qtpy.QtWidgets import QWidget, QLayout


class UIObject():
    def __new__(cls: type["UIObject"]) -> "UIObject":
        try:
            cls._ui_file
        except AttributeError:
            raise NotImplementedError(f'Instantiation of class "{cls.__name__}" failed because it is missing its "_ui_file" string.\nMake sure you\'re not instantiating a base class.')

        return super().__new__(cls)


def loadUi(ui_file_path: str, base_class: type[QWidget] = QWidget, expected_layout: type[QLayout] = QLayout) -> QWidget:
    import qtpy.uic

    ui = qtpy.uic.loadUi(ui_file_path)
    if not isinstance(ui, base_class):
        raise ValueError(
            f'The root element of "{ui_file_path}" should be "{base_class.__name__}", not "{type(ui).__name__}"'
        )

    if not isinstance(ui.layout(), expected_layout):
        raise ValueError(
            f'The layout of "{ui_file_path}" should be "{expected_layout.__name__}", not "{type(ui.layout()).__name__}"'
        )

    return ui
