from qtpy.QtGui import QGuiApplication

def clear():
    QGuiApplication.clipboard().clear()

def copy_text(data):
    QGuiApplication.clipboard().setText(data)

def paste_text():
    return QGuiApplication.clipboard().text()

