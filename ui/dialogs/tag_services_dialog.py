import backend.settings
from .forms import FormDialog
from ui.icons import Icons
from qtpy.QtWidgets import QCheckBox


class TagServicesDialog(FormDialog):
    _form_ui_file = "ui/dialogs/templates/forms/tag_services.ui"

    def __init__(self, tag_services: list[str] = []) -> None:
        super().__init__("Configure Tag Services", "Be careful with the main tag service choice, as it will be the one where we apply/remove tags.", icon=Icons.TAG_ORANGE)

        self.add_save_button()
        self.add_cancel_button()

        if len(tag_services) == 0:
            tag_services = [x.name for x in backend.hydrus.client().get_services().local_tags]

        settings = backend.settings.get_settings()

        self.additional_tag_services = set(settings.additional_tag_services)

        self.form.main_tag_service.addItems(tag_services)
        self.form.main_tag_service.setCurrentText(settings.main_tag_service)

        for service in tag_services:
            checkbox = QCheckBox(service, self.form)
            checkbox.setChecked(service in self.additional_tag_services)
            checkbox.toggled.connect(lambda checked, s=service: self._update_additional_tag_services(s, checked))
            self.form.additional_tag_services.addWidget(checkbox)

    def _update_additional_tag_services(self, service: str, checked: bool) -> None:
        if checked:
            self.additional_tag_services.add(service)
        else:
            self.additional_tag_services.remove(service)

    def _on_save(self) -> None:
        settings = backend.settings.get_settings()

        settings.main_tag_service = self.form.main_tag_service.currentText()
        settings.additional_tag_services = list(self.additional_tag_services)

        settings.save_to_file()
