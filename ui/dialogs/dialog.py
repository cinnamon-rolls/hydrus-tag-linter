import typing as T
import backend.hydrus
import ui
import ui.helper
from ui.icons import Icons
from qtpy.QtCore import Qt
from qtpy.QtGui import QIcon
from qtpy.QtWidgets import QBoxLayout, QDialog, QDialogButtonBox, QPushButton


class Dialog(ui.UIObject):
    def __new__(cls: type["Dialog"]) -> "Dialog":
        obj = super().__new__(cls)
        obj._dialog = ui.loadUi(cls._ui_file, QDialog, QBoxLayout)

        return obj

    def exec(self) -> QDialog.DialogCode:
        self._on_exec()
        return self.dialog.exec()

    def _on_exec(self) -> None:
        pass

    @property
    def dialog(self) -> QDialog:
        return self._dialog

    @property
    def layout(self) -> QBoxLayout:
        return self.dialog.layout()


class MessageDialog(Dialog):
    _ui_file = "ui/dialogs/templates/base_dialog.ui"

    def __new__(cls: type["Dialog"], *args, **kwargs) -> "Dialog":
        obj = super().__new__(cls)

        obj._button_box = ui.helper.find_child(obj._dialog, QDialogButtonBox, "button_box")
        if obj._button_box is None:
            raise AttributeError(f'Dialog UI file "{cls._ui_file}" did not contain a "QDialogButtonBox" object.')

        obj.button_box.helpRequested.connect(obj._on_help_requested)
        obj.button_box.accepted.connect(obj._on_accepted)
        obj.button_box.rejected.connect(obj._on_rejected)

        return obj

    def __init__(self, title: str, text: T.Optional[str], center_text: bool = True, icon: QIcon = None) -> None:
        self.dialog.setWindowTitle(title)
        self.dialog.title.setText(title)

        if text:
            self.dialog.text.setText(text)
        else:
            self.dialog.text.setHidden(True)

        if center_text:
            self.dialog.text.setAlignment(Qt.AlignCenter)

        if icon is None:
            icon = self._get_icon()

        if icon is not None:
            self.dialog.icon.setPixmap(icon.pixmap(Icons.P_ICON_SIZE))

    def _add_button(self, text: str, role: QDialogButtonBox.ButtonRole, icon: QIcon = None) -> T.Optional[QPushButton]:
        # Button order in QButtonBox depends on the role and the OS running the program.
        # If two buttons have the same role, order of addition is used.
        btn = self.button_box.addButton(text, role)
        if icon is not None:
            btn.setIcon(icon)

        return btn

    def add_ok_button(
        self,
        role: QDialogButtonBox.ButtonRole = QDialogButtonBox.AcceptRole
    ) -> T.Optional[QPushButton]:
        return self._add_button("Ok", role, Icons.ACCEPT)

    def add_cancel_button(
        self,
        role: QDialogButtonBox.ButtonRole = QDialogButtonBox.RejectRole
    ) -> T.Optional[QPushButton]:
        return self._add_button("Cancel", role, Icons.CANCEL)

    def add_close_button(
        self,
        role: QDialogButtonBox.ButtonRole = QDialogButtonBox.RejectRole
    ) -> T.Optional[QPushButton]:
        return self._add_button("Close", role, Icons.CROSS)

    def add_yes_button(
        self,
        role: QDialogButtonBox.ButtonRole = QDialogButtonBox.YesRole
    ) -> T.Optional[QPushButton]:
        return self._add_button("Yes", role, Icons.TICK)

    def add_no_button(
        self,
        role: QDialogButtonBox.ButtonRole = QDialogButtonBox.NoRole
    ) -> T.Optional[QPushButton]:
        return self._add_button("No", role, Icons.CROSS)

    def add_save_button(
        self,
        role: QDialogButtonBox.ButtonRole = QDialogButtonBox.AcceptRole
    ) -> T.Optional[QPushButton]:
        # AcceptRole makes it close the dialog, calling _on_accepted() first, then _on_save().
        # ApplyRole would not close the dialog, and only call _on_save().
        # ActionRole is a catch-all role that I haven't tested here. - Laure
        btn = self._add_button("Save", role, Icons.SAVE)
        btn.clicked.connect(self._on_save)
        return btn

    def add_retry_button(
        self,
        role: QDialogButtonBox.ButtonRole = QDialogButtonBox.AcceptRole
    ) -> T.Optional[QPushButton]:
        btn = self._add_button("Retry", role, Icons.ARROW_REFRESH)
        btn.clicked.connect(self._on_retry)
        return btn

    def add_help_button(
        self,
        role: QDialogButtonBox.ButtonRole = QDialogButtonBox.HelpRole
    ) -> T.Optional[QPushButton]:
        return self._add_button("Help", role, Icons.HELP)

    # TODO: document all these functions.
    def _on_exec(self) -> None:
        if len(self.button_box.buttons()) == 0:
            self.add_ok_button()

    def _on_help_requested(self) -> None:
        pass

    def _on_accepted(self) -> None:
        pass

    def _on_rejected(self) -> None:
        pass

    def _on_save(self) -> None:
        pass

    def _on_retry(self) -> None:
        pass

    # Qt built-in MessageBox icons can be obtained with
    # style.standardIcon(QStyle.SP_MessageBoxCritical)
    def _get_icon(self) -> QIcon:
        return None

    @property
    def button_box(self) -> QDialogButtonBox:
        return self._button_box


class InformationDialog(MessageDialog):
    def _get_icon(self) -> QIcon:
        return Icons.INFORMATION


class AboutDialog(MessageDialog):
    def _get_icon(self) -> QIcon:
        return Icons.INFORMATION


class WarningDialog(MessageDialog):
    '''Meant for expected exceptions, things that can happen under known cirumstances.
        For example, a user tries to tag a file but they don't have a tag service selected.'''
    def _get_icon(self) -> QIcon:
        return Icons.ERROR


class ErrorDialog(MessageDialog):
    '''Meant for unexpected exceptions, that should be shown to the user as a way of helping them report bugs.'''
    def __init__(self, title: str, text: T.Optional[str], center_text: bool = False, icon: QIcon = None) -> None:
        super().__init__(title, text, center_text, icon)

    def _get_icon(self) -> QIcon:
        return Icons.EXCLAMATION


class ErrorConnectingDialog(WarningDialog):
    '''Lets the user retry connecting, or update their credentials.'''
    def __init__(self, exception: backend.hydrus.HydrusConnectionError, icon: QIcon = None) -> None:
        # TODO: Hide/Collapse the exception stacktrace by default, it can look unnecesarily scary.
        super().__init__("Error connecting to Hydrus", str(exception), False, icon)

        open_credentials_button = self._add_button("Update credentials", QDialogButtonBox.RejectRole, Icons.COG)
        open_credentials_button.clicked.connect(self._open_credentials)
        self.add_retry_button()
        self.add_cancel_button()

    def _open_credentials(self) -> None:
        from ui.dialogs.hydrus_credentials import HydrusConnectDialog
        HydrusConnectDialog().exec()

    def _on_retry(self) -> None:
        try:
            backend.hydrus.client().lazy_connect()
        except backend.hydrus.HydrusConnectionError as e:
            # TODO: Use an external function to do this recursion
            ErrorConnectingDialog(e).exec()


class QuestionDialog(MessageDialog):
    def __init__(self, title: str, text: T.Optional[str], center_text: bool = True, icon: QIcon = None) -> None:
        super().__init__(title, text, center_text, icon)
        self.add_yes_button()
        self.add_no_button()

    def _get_icon(self) -> QIcon:
        return Icons.QUESTION
