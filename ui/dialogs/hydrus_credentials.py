import hydrus_api # TODO: use HydrusConnectionError.
import backend.hydrus
from .forms import FormDialog
from ui.dialogs import InformationDialog, WarningDialog, ErrorDialog
from ui.icons import Icons
from qtpy.QtGui import QDesktopServices
from qtpy.QtWidgets import QDialogButtonBox

class HydrusConnectDialog(FormDialog):
    _form_ui_file = "ui/dialogs/templates/forms/hydrus_credentials.ui"

    def __init__(self) -> None:
        text = "Make sure the Client API is running and reachable from your device.\nIf you need help with this click the Help button."
        super().__init__("Configure Hydrus API Credentials", text, icon = Icons.CONNECT)

        self.form.api_url.setPlaceholderText(backend.hydrus.DEFAULT_API_URL)

        self.add_save_button()
        self.add_close_button()
        self.add_help_button()

        self.connect_button = self._add_button("Connect", QDialogButtonBox.ButtonRole.ApplyRole, Icons.CONNECT)
        self.connect_button.clicked.connect(self._connect_to_hydrus)

        self.form.access_key.textChanged.connect(self._update_button_text)

    def _update_button_text(self, text: str) -> None:
        self.connect_button.setText("Connect" if text else "Request API Key")

    def _connect_to_hydrus(self) -> None:
        print("CONNECT")

    def _on_exec(self) -> None:
        settings = backend.settings.get_settings()

        access_key = settings.hydrus_access_key

        api_url = settings.hydrus_api_url
        if api_url == backend.hydrus.DEFAULT_API_URL:
            api_url = ""

        self.form.access_key.setText(access_key)
        self.form.api_url.setText(api_url)
        self.form.auto_reconnect.setChecked(settings.hydrus_auto_reconnect)

        self._update_button_text(access_key)

    def _on_save(self) -> None:
        access_key = self.form.access_key.text().strip()
        api_url = self.form.api_url.text().strip()
        auto_reconnect = self.form.auto_reconnect.isChecked()

        settings = backend.settings.get_settings()

        settings.hydrus_access_key = access_key
        settings.hydrus_api_url = api_url
        settings.hydrus_auto_reconnect = auto_reconnect

        settings.save_to_file()

    def _connect_to_hydrus(self) -> None:
        access_key = self.form.access_key.text().strip()
        api_url = self.form.api_url.text().strip()

        if not api_url:
            api_url = backend.hydrus.DEFAULT_API_URL

        if not access_key:
            #TODO: Make a different Dialog for this.
            access_key = self._request_key(api_url)
            self.form.access_key.setText(access_key)
            return

        try:
            backend.hydrus.client().connect(access_key, api_url)
        except backend.hydrus.HydrusConnectionError as e:
            WarningDialog("Connection Failed", str(e)).exec()
        else:
            InformationDialog("Connection Success", "Connected to Hydrus API succesfully.").exec()

    def _request_key(self, api_url: str) -> str:
        #TODO: I was drunk or something. - laure
        try:
            access_key = backend.hydrus.client().request_api_key(api_url)
        except hydrus_api.APIError as e:
            WarningDialog("API Error", f"The API responded with the following message: {str(e)}").exec()
        except hydrus_api.ConnectionError:
            WarningDialog("Connection Error", f"Connection Error: There was a problem trying to connect to the API at {api_url}, make sure the URL is correct.").exec()
        except Exception as e:
            ErrorDialog("Error", f"{type(e).__name__}: " + str(e)).exec()
        else:
            InformationDialog("Connection Success", "Click OK after you're accepted the permissions on Hydrus.").exec()
            return access_key

    def _on_help_requested(self) -> None:
        doc_url = "https://hydrusnetwork.github.io/hydrus/client_api.html"
        if not QDesktopServices.openUrl(doc_url):
            InformationDialog("Couldn't open web browser", f"Coudln't open web browser to Hydrus' help page. Here's the URL:\n{doc_url}").exec()
