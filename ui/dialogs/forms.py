import typing as T
import ui
from .dialog import MessageDialog
from qtpy.QtGui import QIcon
from qtpy.QtWidgets import QWidget, QFormLayout

class FormDialog(MessageDialog):
    _form_ui_file: str = None

    def __new__(cls: type["FormDialog"], *args, **kwargs) -> "FormDialog":
        if cls is FormDialog:
            raise TypeError('Only children of "FormDialog" may be instantiated')

        ui_file = cls._form_ui_file
        if ui_file is None:
            raise AttributeError(f'You forgot to set the UI file path of "{cls.__name__}", you idiot.')

        obj = super().__new__(cls)
        obj._form = ui.loadUi(ui_file, QWidget, QFormLayout)

        obj.layout.insertWidget(obj.layout.indexOf(obj.dialog.text) + 1, obj._form)

        return obj

    def __init__(self, title: str, text: T.Optional[str], center_text: bool = False, icon: QIcon = None) -> None:
        super().__init__(title, text, center_text, icon)

    @property
    def form(self) -> QWidget:
        return self._form