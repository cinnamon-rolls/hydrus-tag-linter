from qtpy.QtWidgets import QStackedWidget, QMainWindow, QMenu, QMenuBar
from .pages import HomePage, SlideshowPage, SettingsPage, Page
from backend.slideshow import Slideshow


class MainWindow(QMainWindow):
    def __init__(self) -> None:
        super().__init__()

        self.setWindowTitle("Hydrus Tag Helper")
        self.resize(800, 600)
        self.setMinimumSize(640, 480)

        self.pages = []
        self.idx = 0

        self.stack = QStackedWidget()
        self.setCentralWidget(self.stack)

    def update_menu_bar(self) -> None:
        bar = QMenuBar(self)

        bar.addMenu(self._create_nav_menu(bar))

        for i in self.current_page.get_menus():
            if isinstance(i, QMenu):
                bar.addMenu(i)
            else:
                bar.addAction(i)

        self.setMenuBar(bar)

    def _create_nav_menu(self, bar: QMenuBar) -> QMenu:
        nav = QMenu("Navigate", bar)

        if not isinstance(self.current_page, HomePage):
            nav.addAction("Home").triggered.connect(self.go_home)

        #TODO: Temporarily disabled, as the hydrus credentials can now be set from a dialog.
        # if not isinstance(self.current_page, SettingsPage):
        #     nav.addAction("Settings").triggered.connect(self.go_settings)

        self.nav_separator = nav.addSeparator()
        nav.addAction("Exit").triggered.connect(self.close)

        return nav

    @property
    def current_page(self) -> Page:
        return self.pages[self.idx]

    def add_default_pages(self) -> None:
        self.pages = [
            HomePage(),
            SettingsPage(),
            SlideshowPage()
        ]

        for page in self.pages:
            self.stack.addWidget(page.widget)

        self.go_home()

    def go_home(self) -> None:
        self._go_to_page(0)

    def go_settings(self) -> None:
        self._go_to_page(1)

    def go_to_slideshow_page(self, slideshow: Slideshow) -> None:
        if not isinstance(slideshow, Slideshow):
            raise TypeError("Attempt to go to Slideshow page with object of type " + type(slideshow).__name__)

        self.pages[2].set_slideshow(slideshow)
        self._go_to_page(2)

    def _go_to_page(self, index: int) -> None:
        self.current_page.unload()
        self.idx = index
        self.stack.setCurrentIndex(self.idx)
        self.update_menu_bar()
        self.current_page.load()


__main_window = None


def create_main_window() -> MainWindow:
    global __main_window
    if __main_window is not None:
        raise RuntimeError("A MainWindow already exists!")
    __main_window = MainWindow()
    __main_window.add_default_pages()
    __main_window.show()
    return __main_window


def main_window() -> MainWindow:
    if __main_window is None:
        raise RuntimeError("MainWindow is not defined yet! Use ui.create_main_window().")
    return __main_window
