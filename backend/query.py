from backend.hydrus import HydrusSearchRunner
from backend.slideshow import Slideshow

# A Query is something that returns a list of file IDs
# It may consist of zero or more component queries


class Query:
    def __init__(self):
        pass

    def run_search(self, runner: HydrusSearchRunner) -> list[int]:
        raise NotImplementedError()


class EmptyQuery(Query):
    def __init__(self):
        pass

    def run_search(self, runner: HydrusSearchRunner):
        return []


class EverythingQuery(Query):
    def __init__(self):
        pass

    def run_search(self, runner: HydrusSearchRunner):
        # take advantage of a quirk in the API - when you ask for an empty set of tags, the API returns all files
        return runner.build_and_execute([])


class TagsQuery(Query):
    def __init__(self, tags: list[str]):
        self.tags = tags

    def run_search(self, runner: HydrusSearchRunner):
        return runner.build_and_execute(self.tags)


class OrQuery(Query):
    def __init__(self, queries: list[Query]):
        self.queries = queries

    def run_search(self, runner: HydrusSearchRunner):
        already = set()
        ret = []

        for query in self.queries:
            for result in query.run_search(runner):
                if not result in already:
                    already.insert(result)
                    ret.append(result)

        return ret


def create_query(data):
    if data is None:
        return EmptyQuery()
    elif isinstance(data, list):
        tags = []
        queries = []

        for i in data:
            if isinstance(i, Query):
                queries.append(i)
            else:
                # TODO: we're still assuming that 'data' is of
                # type list[str | list[str]] (notice the nesting).
                tags.append(i)

        tags_query = TagsQuery(tags)

        if len(queries) == 0:
            return tags_query

        return OrQuery(queries + tags_query)

    elif isinstance(data, bool):
        if data:
            return EverythingQuery()
        else:
            return EmptyQuery()

    raise TypeError(f"Not sure how to make Query from {type(data).__name__}")
