from backend.hydrus import HydrusClient, TAG_ACTION_ADD, TAG_ACTION_DELETE
from backend.slideshow import Slideshow
import backend.settings
from type_checking import require_bool, require_list, require_str
import typing as T
from qtpy.QtWidgets import QInputDialog, QLineEdit

# Shortcuts for the first 10 custom actions defined by a rule.
# TODO: Make configurable use of shift, or disable auto-shortcuts entirely.
RULE_ACTION_SHORTCUTS = {i - 11: f"{i}"[1:] for i in range(11, 21)}


class ActionContext:
    def __init__(self,
                 client: HydrusClient,
                 slideshow: Slideshow,
                 tag_service_name: T.Optional[str] = None,
                 tag_service_key: T.Optional[str] = None):

        if tag_service_name is None and tag_service_key is None:
            raise ValueError("Expected one of either tag_service_name or tag_service_key")

        self.client = client
        self.slideshow = slideshow
        self.tag_service_name = tag_service_name
        self.tag_service_key = tag_service_key

    @property
    def file(self):
        if self.slideshow is None:
            return None
        return self.slideshow.get_current_file(self.client)

    @property
    def has_file(self):
        return self.file is not None

    @property
    def file_hash(self):
        if self.file is None:
            return None
        return self.file.hash

    @property
    def has_tag_service(self):
        return self.tag_service_name is not None or self.tag_service_key is not None

    # TODO I'm not sure if this is the best place to put this method
    # Returns an error message, or None upon success
    def change_tags(self, add_tags, remove_tags) -> T.Optional[str]:

        if self.client is None:
            return "No Hydrus client to change tags with"

        if not self.has_tag_service:
            return "No tag service information"

        if self.tag_service_name is None:
            return "(TODO) currently only tag service name is supported, not key"  # TODO

        the_hash = self.file_hash

        if the_hash is None:
            return "No file hash to add/remove tags to"

        att = {}

        if len(add_tags) == 0 and len(remove_tags) == 0:
            return None

        if len(add_tags) > 0:
            att[TAG_ACTION_ADD] = add_tags

        if len(remove_tags) > 0:
            att[TAG_ACTION_DELETE] = remove_tags

        sntatt = {}
        sntatt[self.tag_service_name] = att

        self.client.change_tags(hashes=[the_hash], service_names_to_actions_to_tags=sntatt)

        return None


class ActionResult:
    def __init__(self, success: bool = False, should_advance: bool = False, message: str = None):
        self.success = success
        self.should_advance = should_advance
        self.message = message

    def __repr__(self):
        return f"(success={self.success}, should_advance={self.should_advance}, message={self.message})"


class Action:
    def run(self, ctx: ActionContext) -> ActionResult:
        raise NotImplementedError()

    def get_name(self) -> str:
        raise NotImplementedError()

    def get_shortcut(self) -> T.Optional[str]:
        return None

    def is_hidden(self, ctx: ActionContext) -> bool:
        return False

    def is_enabled(self, ctx: ActionContext) -> bool:
        return not self.is_hidden(ctx)


# An action that does nothing and always succeeds
class NonAction(Action):
    def get_name(self) -> str:
        return "Do nothing"

    def run(self, ctx: ActionContext) -> ActionResult:
        return ActionResult(success=True)

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


class MoveToTrashAction(Action):
    def get_name(self) -> str:
        return "Move to trash"

    def get_shortcut(self) -> T.Optional[str]:
        return "d"

    def is_hidden(self, ctx: ActionContext) -> bool:
        return ctx.file.is_in_trash

    def run(self, ctx: ActionContext) -> bool:
        backend.hydrus.client().delete_files([ctx.file_hash])
        return ActionResult(success=True)

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


class MoveOutOfTrashAction(Action):
    def get_name(self) -> str:
        return "Move out of trash"

    def get_shortcut(self) -> T.Optional[str]:
        return "shift+d"

    def is_hidden(self, ctx: ActionContext) -> bool:
        return not ctx.file.is_in_trash

    def run(self, ctx: ActionContext) -> bool:
        backend.hydrus.client().undelete_files([ctx.file_hash])
        return ActionResult(success=True)

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


class MoveToInboxAction(Action):
    def get_name(self) -> str:
        return "Move to inbox"

    def get_shortcut(self) -> T.Optional[str]:
        return "shift+a"

    def is_hidden(self, ctx: ActionContext) -> bool:
        return ctx.file.is_in_inbox

    def run(self, ctx: ActionContext):
        backend.hydrus.client().unarchive_files([ctx.file_hash])
        return ActionResult(success=True)

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


class MoveToArchiveAction(Action):
    def get_name(self) -> str:
        return "Move to archive"

    def get_shortcut(self) -> T.Optional[str]:
        return "a"

    def is_hidden(self, ctx: ActionContext) -> bool:
        return ctx.file.is_in_archive

    def run(self, ctx: ActionContext):
        backend.hydrus.client().archive_files([ctx.file_hash])
        return ActionResult(success=True)

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


class MarkAsExemptAction(Action):
    def get_name(self) -> str:
        return "Mark as exempt (NOT IMPLEMENTED)"

    def get_shortcut(self) -> T.Optional[str]:
        return "e"

    def run(self, ctx: ActionContext) -> bool:
        return ActionResult(success=False, message="Not implemented")

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


class UnmarkAsExemptAction(Action):
    def get_name(self) -> str:
        return "Unmark as exempt (NOT IMPLEMENTED)"

    def get_shortcut(self) -> T.Optional[str]:
        return "shift+e"

    def run(self, ctx: ActionContext) -> bool:
        return ActionResult(success=False, message="Not implemented")

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


class SkipAction(Action):
    def get_name(self) -> str:
        return "Skip"

    def get_shortcut(self) -> T.Optional[str]:
        return "s"

    def run(self, ctx: ActionContext) -> bool:
        ctx.slideshow.move_to_next_file()
        return ActionResult(success=True)

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


class UnskipAction(Action):
    def get_name(self) -> str:
        return "Go back"

    def get_shortcut(self) -> T.Optional[str]:
        return "shift+s"

    def run(self, ctx: ActionContext) -> bool:
        ctx.slideshow.move_to_previous_file()
        return ActionResult(success=True)

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass()


# An action that runs 0 or more other actions in order until it runs out of
# actions or one fails. If it could get through all actions without a failure,
# then it succeeds.
class MultiAction:
    def __init__(self, actions: list[Action]):
        self.actions = actions

    def get_name(self) -> str:
        return ', then '.join([x.get_name() for x in self.actions])

    def run(self, ctx: ActionContext) -> ActionResult:
        result = ActionResult(success=True)
        for action in self.actions:
            result = action.run(ctx)
            if not result.success:
                return result
        return result


class PresetTagAction(Action):
    def __init__(self, add_tags: list[str], remove_tags: list[str]):
        self.add_tags = add_tags
        self.remove_tags = remove_tags

    def get_name(self) -> str:
        if len(self.add_tags) == 0 and len(self.remove_tags) == 0:
            return "Do nothing (add/remove 0 tags)"

        joiner = "', '"
        name = ""

        if len(self.add_tags) > 0:
            if len(self.add_tags) == 1:
                name = f"Add tag '{self.add_tags[0]}'"
            else:
                name = f"Add tags '{joiner.join(self.add_tags)}'"

        if len(self.remove_tags) > 0:

            if name != "":
                name = name + " and remove "
            else:
                name = name + "Remove "

            if len(self.remove_tags) == 1:
                name = name + f"tag '{self.remove_tags[0]}'"
            else:
                name = name + f"tags '{joiner.join(self.remove_tags)}'"

        return name

    def run(self, ctx: ActionContext) -> ActionResult:
        if len(self.add_tags) == 0 and len(self.remove_tags) == 0:
            return ActionResult(success=True, message="No tags added")

        msg = ctx.change_tags(add_tags=self.add_tags, remove_tags=self.remove_tags)

        if msg is None:
            return ActionResult(success=True)
        else:
            return ActionResult(success=False, message=msg)

    @classmethod
    def from_raw(selfclass, data: dict):
        add_tags = require_list("add_tags", data, str)
        remove_tags = require_list("remove_tags", data, str)
        return selfclass(add_tags, remove_tags)


class ActionDecorator(Action):
    def __init__(self, child: Action):
        self._child = child

    def run(self, ctx: ActionContext) -> ActionResult:
        ret = self._child.run(ctx)
        return ret

    def get_name(self) -> str:
        return self._child.get_name()

    def get_shortcut(self) -> T.Optional[str]:
        return self._child.get_shortcut()

    def is_hidden(self, ctx: ActionContext) -> bool:
        return self._child.is_hidden(ctx)

    def is_enabled(self, ctx: ActionContext) -> bool:
        return self._child.is_enabled(ctx)


class ActionOverwriteName(ActionDecorator):
    def __init__(self, child: Action, new_name: str):
        super().__init__(child)
        self.new_name = new_name

    def get_name(self):
        return self.new_name


class ActionOverwriteSuccess(ActionDecorator):
    def __init__(self, child: Action, overwrite):
        super().__init__(child)
        self.overwrite = overwrite

    def run(self, ctx: ActionContext) -> ActionResult:
        result = self._child.run(ctx)
        result.success = self.overwrite
        return result


class ActionOverwriteShouldAdvance(ActionDecorator):
    def __init__(self, child: Action, advance_on_fail=False, advance_on_success=False):
        super().__init__(child)
        self.advance_on_fail = advance_on_fail
        self.advance_on_success = advance_on_success

    def run(self, ctx: ActionContext):
        result = self._child.run(ctx)
        if result.success:
            result.should_advance = self.advance_on_success
        else:
            result.should_advance = self.advance_on_fail
        return result


class ActionOverwriteShortcut(ActionDecorator):
    def __init__(self, child: Action, new_shortcut: str):
        super().__init__(child)
        self.new_shortcut = new_shortcut

    def get_shortcut(self) -> T.Optional[str]:
        return self.new_shortcut


# Action that pops up a dialog allowing the user to enter zero or more tags
class TagPromptAction(Action):
    def __init__(self, is_action_add: bool) -> None:
        self.is_action_add = is_action_add

    def get_name(self):
        if self.is_action_add:
            return "Quick add tag"
        else:
            return "Quick remove tag"

    def run(self, ctx: ActionContext) -> bool:
        title = "Apply Tags" if self.is_action_add else "Remove Tags"
        text, ok = QInputDialog.getText(None, title, f"Provide a list of comma-separated tags", QLineEdit.Normal)
        if ok:
            tags = [x.strip() for x in text.split(",")]
        else:
            return ActionResult(success=False)

        if self.is_action_add:
            add_tags = tags
            remove_tags = []
        else:
            add_tags = []
            remove_tags = tags

        msg = ctx.change_tags(add_tags=add_tags, remove_tags=remove_tags)

        if msg is not None:
            return ActionResult(success=True)
        else:
            return ActionResult(success=False, message=msg)

    def get_shortcut(self) -> T.Optional[str]:
        if self.is_action_add:
            return '='
        else:
            return '-'

    @classmethod
    def from_raw(selfclass, data: dict):
        return selfclass(data.get("adding_tags", True))


ACTION_TEMPLATES = {
    "default": NonAction.from_raw,
    "tag_prompt": TagPromptAction.from_raw,
    "tag": PresetTagAction.from_raw,
    "move_to_trash": MoveToTrashAction.from_raw,
    "move_to_inbox": MoveToInboxAction.from_raw,
    "move_to_archive": MoveToArchiveAction.from_raw,
    "mark_as_exempt": MarkAsExemptAction.from_raw,
    "unmark_as_exempt": UnmarkAsExemptAction.from_raw,
    "skip": SkipAction.from_raw,
    "unskip": UnskipAction.from_raw,
}


class ActionCreationError(RuntimeError):
    def __init__(self, message):
        super().__init__(message)


def create_action(data: dict) -> Action:
    template_str = require_str("template", data, required=True, lower=True, default="default")

    if template_str not in ACTION_TEMPLATES:
        raise ActionCreationError(
            f'Action template "{template_str}"" is not valid. Here\'s a list of supported templates: {ACTION_TEMPLATES.keys()}'
        )

    template_func = ACTION_TEMPLATES[template_str]

    try:
        return template_func(data)
    except NotImplementedError as e:
        raise ActionCreationError(
            f"Encountered unimplemented functionality while creating action of type '{template_str}'")
    except ActionCreationError as e:
        raise ActionCreationError(f"Failed to create Action of type '{template_str}'")

COMMON_ACTIONS = [
    SkipAction(),
    UnskipAction(),
    TagPromptAction(True),
    TagPromptAction(False),
]

COMMON_ACTIONS_FOR_RULES = [
    *COMMON_ACTIONS,
    MoveToTrashAction(),
    MoveOutOfTrashAction(),
    MoveToInboxAction(),
    MoveToArchiveAction(),
    MarkAsExemptAction(),
    UnmarkAsExemptAction(),
]
