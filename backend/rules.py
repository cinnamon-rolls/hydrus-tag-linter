import json
from backend.hydrus import HydrusSearchRunner
from backend.actions import (
    Action,
    PresetTagAction,
    ActionOverwriteShortcut,
    ActionOverwriteName,
    ActionOverwriteShouldAdvance,
    RULE_ACTION_SHORTCUTS,
    create_action
)
from backend.query import TagsQuery, Query, create_query
from pathlib import Path
from type_checking import require_bool, require_list, require_str


class RuleInfo():
    def __init__(self):
        self.file_path = None
        self.uid = None
        self.name = None
        self.description = None

    @classmethod
    def from_raw(selfclass, data: dict, file_path: str = None) -> "RuleInfo":
        if not isinstance(data, dict):
            raise RuleCreationError(f"Expected a dictionary, but got instead: {type(data).__name__}")

        info = selfclass()

        info.name = require_str("name", data, "Unnamed Rule")
        info.description = require_str("desc", data, "No description provided.")
        info.uid = require_str("uid", data, None)
        info.file_path = file_path

        return info


class Rule():
    def __init__(self, info: RuleInfo) -> None:
        self.info = info

    def run_search(self, runner: HydrusSearchRunner):
        raise NotImplementedError()

    def get_actions(self) -> list:
        raise NotImplementedError()


class EmptyRule(Rule):
    def __init__(self, info: RuleInfo):
        super().__init__(info)

    def run_search(self, runner: HydrusSearchRunner):
        return []

    def get_actions(self) -> list:
        return []


class QueryRule(Rule):
    def __init__(self, info: RuleInfo, query: Query):
        super().__init__(info)
        self.query = query

    def run_search(self, runner: HydrusSearchRunner):
        return self.query.run_search(runner)

    def get_actions(self) -> list:
        return []

    @classmethod
    def from_raw(selfclass, info, data):
        query_raw = data.get("query", data.get("search", None))
        return selfclass(info, create_query(query_raw))

    @classmethod
    def from_tags(selfclass, info, tags):
        return selfclass(info, TagsQuery(tags))


class RuleDecorator(Rule):
    def __init__(self, child: Rule):
        super().__init__(child.info)
        self.child = child

    def run_search(self, runner: HydrusSearchRunner):
        return self.child.run_search(runner)

    def get_actions(self) -> list:
        return self.child.get_actions()


class RuleActionAdder(RuleDecorator):
    def __init__(self, child: Rule, actions: list[Action]):
        super().__init__(child)
        self.actions: list[Action] = actions

    def get_actions(self) -> list:
        return self.actions + self.child.get_actions()

    @classmethod
    def from_raw(selfclass, rule, data):
        if isinstance(data, dict):
            return selfclass(rule, [create_action(x) for x in data.get("actions", [])])
        elif isinstance(data, list):
            return selfclass(rule, [create_action(x) for x in data])
        else:
            raise TypeError(type(data).__name__)


def create_tag_disambiguation_rule(info, data: dict):
    bad_tags = require_list("bad_tags", data, item_type=str, required=True)
    alternatives = require_list("alternatives", data, item_type=str, default=[])

    ret = QueryRule.from_tags(info, bad_tags)

    actions = []
    for i, alternative in enumerate(alternatives):
        action = PresetTagAction(remove_tags=bad_tags, add_tags=[alternative])
        action = ActionOverwriteName(action, f"Replace with '{alternative}'")
        action = ActionOverwriteShortcut(action, RULE_ACTION_SHORTCUTS.get(i, ""))
        action = ActionOverwriteShouldAdvance(action, advance_on_success=True)
        actions.append(action)

    ret = RuleActionAdder(ret, actions)
    return ret


def create_mutual_exclusion_rule(info, data: dict) -> Rule:
    tags = require_list("tags", data, item_type=str, required=True)

    # The actual "mutual exclusion" boolean logic, built only using a
    # single AND consisting of n_tags+1 OR expressions.
    # I used http://tma.main.jp/logic/index_en.html - Laure
    search = [tags]
    for i in range(len(tags)):
        l = tags.copy()
        l[i] = "-" + l[i]
        search.append(l)

    ret = QueryRule.from_tags(info, search)

    actions = []
    for i, tag in enumerate(tags):
        bad_tags = tags.copy()
        bad_tags.remove(tag)

        action = PresetTagAction(remove_tags=bad_tags, add_tags=[tag])
        action = ActionOverwriteName(action, f"Keep '{tag}'")
        action = ActionOverwriteShortcut(action, RULE_ACTION_SHORTCUTS.get(i, ""))
        action = ActionOverwriteShouldAdvance(action, advance_on_success=True)
        actions.append(action)

    ret = RuleActionAdder(ret, actions)
    return ret


def create_require_one_rule(info, data):
    tags = require_list("tags", data, item_type=str, required=True)

    search = require_list("search", data, default=[])
    search.extend([f"-{x}" for x in tags])
    ret = QueryRule.from_tags(info, search)

    actions = []
    for i, tag in enumerate(tags):
        action = PresetTagAction(add_tags=[tag], remove_tags=[])
        action = ActionOverwriteShortcut(action, RULE_ACTION_SHORTCUTS.get(i, ""))
        action = ActionOverwriteShouldAdvance(action, advance_on_success=True)
        actions.append(action)

    ret = RuleActionAdder(ret, actions)
    return ret


def create_helicopter_parent_rule(info, data):
    parent = require_str("parent", data)
    children = require_list("children", data, item_type=str)

    search = [parent]
    actions = []

    action = PresetTagAction(add_tags=[], remove_tags=[parent])
    action = ActionOverwriteShortcut(action, RULE_ACTION_SHORTCUTS.get(0, ""))
    action = ActionOverwriteShouldAdvance(action, advance_on_success=True)
    actions.append(action)

    for i, child in enumerate(children):
        search.append(f"-{child}")

        action = PresetTagAction(add_tags=[child], remove_tags=[])
        action = ActionOverwriteShortcut(action, RULE_ACTION_SHORTCUTS.get(i+1, ""))
        action = ActionOverwriteShouldAdvance(action, advance_on_success=True)
        actions.append(action)

    ret = QueryRule.from_tags(info, search)

    ret = RuleActionAdder(ret, actions)
    return ret


RULE_TEMPLATES = {
    "default": QueryRule.from_raw,
    "tag disambiguation": create_tag_disambiguation_rule,
    "require one": create_require_one_rule,
    "helicopter parent": create_helicopter_parent_rule,
    "mutual exclusion": create_mutual_exclusion_rule
}

DECORATOR_TEMPLATES = {"action adder": RuleActionAdder.from_raw}


class RuleCreationError(RuntimeError):
    def __init__(self, message):
        super().__init__(message)


def create_rule(data: dict, file_path: str = None):
    if not isinstance(data, dict):
        raise RuleCreationError(f"Expected a dictionary, but got instead: {type(data).__name__}")

    try:
        info = RuleInfo.from_raw(data, file_path)
    except RuleCreationError as e:
        raise RuleCreationError(f"Error while reading Rule info: {str(e)}")

    template_str = require_str("template", data, default="default", lower=True)

    if template_str not in RULE_TEMPLATES:
        raise RuleCreationError(
            f'Rule template "{template_str}" not found. Here\'s a list of supported templates: {RULE_TEMPLATES}')

    template_func = RULE_TEMPLATES[template_str]

    ret = template_func(info, data)

    decorators = data.get("decorators", [])

    # TODO: improve and use functions from type_checking module instead of isinstance()
    if not isinstance(decorators, list):
        raise RuleCreationError("Got decorators key, but value is not a list")

    for decorator in decorators:

        if not isinstance(decorator, dict):
            raise RuleCreationError("Decorator entry is not a dictionary")

        decorator_template = decorator.get("template", None)

        if not isinstance(decorator_template, str):
            raise RuleCreationError("Decorator entry's template key is not a string")

        if not decorator_template in DECORATOR_TEMPLATES:
            raise RuleCreationError("Decorator template not found: {decorator_template}")

        decorator_func = DECORATOR_TEMPLATES[decorator_template]
        ret = decorator_func(ret, decorator)

        if not isinstance(ret, Rule):
            raise RuleCreationError("Decorator function returned object of type " + type(ret).__name__)

    return ret


def create_rule_from_json_str(json_str : str):
    try:
        data = json.loads(json_str)
    except json.JSONDecodeError as e:
        raise RuleCreationError(f"Invalid JSON: {str(e)}")
    return create_rule(data)

def load_rules_in_dirs(rule_dirs: list[str]) -> list[Rule]:
    if not isinstance(rule_dirs, list):
        raise TypeError(f"Expected list, got {type(rule_dirs).__name__}")

    rules_by_uid = {}

    path_stack = []
    path_stack.extend(rule_dirs)

    while len(path_stack) > 0:
        the_path = path_stack.pop()

        if isinstance(the_path, str):
            the_path = Path(the_path)

        if not isinstance(the_path, Path):
            raise TypeError(f"Expected Path, got {type(the_path).__name__}")

        if not the_path.exists():
            continue

        if the_path.is_dir():
            for contained_file in the_path.iterdir():
                path_stack.append(contained_file)
            continue

        rule_data = None

        if the_path.suffix == ".json":
            with open(the_path, "r") as file:
                rule_data = json.load(file)

        # TODO right now non-json files are ignored
        if rule_data is None:
            continue

        if isinstance(rule_data, dict):
            rule_data = [rule_data]

        if not isinstance(rule_data, list):
            raise RuleCreationError(
                f'Root element of rule definition in {the_path} is not of type "Dictionary" or "List".')

        for rule_def in rule_data:

            if type(rule_def) != dict:
                raise RuleCreationError(
                    f'Error with rule defined in {the_path}.\nRule definitions should be of type "Dictionary".')

            if rule_def.get("disabled", False):
                continue

            try:
                rule = create_rule(rule_def, the_path)
            except RuleCreationError as e:
                raise RuleCreationError(f"Error while reading rule defined in file '{the_path}': {str(e)}")

            if not isinstance(rule, Rule):
                raise RuleCreationError("Rule creation function returned an object of type " + type(rule).__name__)

            if rule.info.uid in rules_by_uid:
                raise RuleCreationError(
                    f"Rule defined in file '{the_path}' has the same uid as rule defined in file '{rules_by_uid[rule.info.uid].file_path}'"
                )

            rules_by_uid[rule.info.uid] = rule

    return [x for x in rules_by_uid.values()]
