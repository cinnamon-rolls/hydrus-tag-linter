from backend.rules import Rule
from backend.slideshow import Slideshow
from backend.hydrus import HydrusClient, HydrusSearchRunner
from backend.actions import COMMON_ACTIONS_FOR_RULES


class RuleSlideshow(Slideshow):
    def __init__(self, rule: Rule):
        super().__init__()
        self._rule = rule

    def get_title(self):
        return self._rule.info.name

    def get_description(self):
        return self._rule.info.description

    def get_actions(self):
        return self._rule.get_actions() + COMMON_ACTIONS_FOR_RULES

    def _subclass_fetch_results(self, search_runner : HydrusSearchRunner) -> list[int]:
        return self._rule.run_search(search_runner)
