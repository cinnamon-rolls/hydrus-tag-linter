from backend.query import Query
from backend.slideshow import Slideshow
from backend.hydrus import HydrusClient, HydrusSearchRunner
from backend.actions import COMMON_ACTIONS


class QuerySlideshow(Slideshow):
    def __init__(
            self,
            query : Query,
            title : str = None,
            description : str = None):
        super().__init__()
        self._query = query
        self._title = title
        self._description = description

    def get_title(self):
        return self._title

    def get_description(self):
        return self._description

    def get_actions(self):
        return COMMON_ACTIONS

    def _subclass_fetch_results(self, search_runner : HydrusSearchRunner) -> list[int]:
        return self._query.run_search(search_runner)
