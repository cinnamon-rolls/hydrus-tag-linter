import json
import typing as T
import os
from backend.hydrus import DEFAULT_API_URL, FILE_SORT_DEFAULT
from copy import deepcopy
from configparser import ConfigParser
from pathlib import Path


class Settings:
    _SECTIONS_TO_KEYS = {
        "HYDRUS": [
            "access_key",
            "api_url",
            "auto_reconnect",
        ],
        "LINTER": [
            "main_tag_service",
            "additional_tag_services",
            "default_file_sort",
            "style_sheet_path",
            "rule_file_paths"
        ],
        "SLIDESHOW": [],
        "TAG_NAMESPACE_COLORS": []
    }

    def __init__(self) -> None:
        self.file_path = Path("config.ini")
        self.config = self._load_from_file()

        self._hydrus_access_key = self._get_str("HYDRUS", "access_key", env_key="HYDRUS_ACCESS_KEY")
        self._hydrus_api_url = self._get_str("HYDRUS", "api_url", env_key="HYDRUS_API_URL")
        self._hydrus_auto_reconnect = self._get_bool("HYDRUS", "auto_reconnect", default=True)

        self._main_tag_service = self._get_str("LINTER", "main_tag_service", env_key="LINTER_MAIN_TAG_SERVICE")
        self._additional_tag_services = self._get_str_list("LINTER", "additional_tag_services", default=[])

        self._default_file_sort = self._get_int("LINTER", "default_file_sort")

        self._style_sheet_path = self._get_str("LINTER", "style_sheet_path")

        self._rule_file_paths = self._get_str_list("LINTER", "rule_file_paths", default=["./my_rules", "./default_rules"])

    def _validate_section_key(self, section: str, key: str) -> None:
        if section not in self._SECTIONS_TO_KEYS:
            raise KeyError(f'Settings: Section "{section} is not defined in the config template."')
        if key not in self._SECTIONS_TO_KEYS[section]:
            raise KeyError(f'Settings: Key "{key}" is not defined for section "{section}".')

    # try to load a setting, with this order of priorities:
    # 1. get it from an environment variable
    # 2. get it from the configparser
    # 3. use a default value (fallback)
    def _get_str(self, section: str, key: str, env_key: T.Optional[str] = None, default: str = "") -> str:
        self._validate_section_key(section, key)
        if env_key is not None:
            env_val = os.getenv(env_key)
            if env_val is not None:
                return env_val

        # TODO: remove quotes and double quotes from the final value, in case the user puts them unnecesarily.
        value = self.config.get(section, key, fallback="")
        if value:  # "" is considered False
            return value
        return default

    def _get_int(self, section: str, key: str, env_key: T.Optional[str] = None, default: int  = -1) -> int:
        text = self._get_str(section=section, key=key, env_key=env_key, default=default)
        try:
            return int(text)
        except ValueError as e:
            return default

    def _get_str_list(self,
                      section: str,
                      key: str,
                      env_key: T.Optional[str] = None,
                      default: list[str] = []) -> list[str]:
        l = self._get_str(section, key, env_key=env_key)
        if l:
            loaded_list = json.loads(l)
            if isinstance(loaded_list, list):
                # TODO: make sure the items in the list are all strings, or replace json with a simpler comma separated parser (preferred).
                return loaded_list
        return default

    def _get_bool(self, section: str, key: str, env_key: T.Optional[str] = None, default: bool = False) -> bool:
        b = self._get_str(section, key, env_key=env_key).lower()
        if b:
            return (b == "1" or b == "true")
        return default

    def _set_str(self, section: str, key: str, value: T.Optional[str]) -> None:
        self._validate_section_key(section, key)

        if value is None:
            value = ""

        self.config[section][key] = value

    def _set_int(self, section: str, key: str, value: T.Optional[int]) -> None:
        self._set_str(section=section, key=key, value=str(value))

    @property
    def hydrus_access_key(self) -> T.Optional[str]:
        if not self._hydrus_access_key:
            return None
        return self._hydrus_access_key

    @property
    def hydrus_api_url(self) -> str:
        if not self._hydrus_api_url:
            return DEFAULT_API_URL
        return self._hydrus_api_url

    @property
    def hydrus_auto_reconnect(self) -> bool:
        return self._hydrus_auto_reconnect

    @property
    def main_tag_service(self) -> T.Optional[str]:
        if not self._main_tag_service:
            return None
        return self._main_tag_service

    @property
    def default_file_sort(self) -> T.Optional[int]:
        if self._default_file_sort is None or self._default_file_sort < 0:
            return None
        return self._default_file_sort

    @property
    def additional_tag_services(self) -> list[str]:
        return self._additional_tag_services

    @property
    def style_sheet_path(self) -> str:
        return self._style_sheet_path

    @property
    def rule_file_paths(self) -> list[str]:
        return self._rule_file_paths

    @hydrus_access_key.setter
    def hydrus_access_key(self, value: str) -> None:
        self._hydrus_access_key = value
        self._set_str("HYDRUS", "access_key", self._hydrus_access_key)

    @hydrus_api_url.setter
    def hydrus_api_url(self, value: T.Optional[str]) -> None:
        self._hydrus_api_url = value
        self._set_str("HYDRUS", "api_url", self._hydrus_api_url)

    @hydrus_auto_reconnect.setter
    def hydrus_auto_reconnect(self, value: bool) -> None:
        self._hydrus_auto_reconnect = value
        self._set_str("HYDRUS", "auto_reconnect", str(self._hydrus_auto_reconnect))

    @main_tag_service.setter
    def main_tag_service(self, value: str) -> None:
        self._main_tag_service = value
        self._set_str("LINTER", "main_tag_service", self._main_tag_service)

    @additional_tag_services.setter
    def additional_tag_services(self, value: list[str]) -> None:
        self._additional_tag_services = value
        self._set_str("LINTER", "additional_tag_services", json.dumps(self._additional_tag_services))

    @default_file_sort.setter
    def default_file_sort(self, value: int) -> None:
        self._default_file_sort = value
        self._set_int("LINTER", "default_file_sort", value)

    @style_sheet_path.setter
    def style_sheet_path(self, value: str):
        self._style_sheet_path = value
        self._set_str("LINTER", "style_sheet_path", value)

    @rule_file_paths.setter
    def rule_file_paths(self, value: list[str]):
        self._rule_file_paths = value
        self._set_list("LINTER", "rule_file_paths", json.dumps(self._rule_file_paths))

    def save_to_file(self) -> None:
        if self.file_path.exists() and not self.file_path.is_file():
            #TODO: Notify the user that the config couldn't be saved.
            return

        # Create a deepcopy of the ConfigParser object without the empty sections and values
        cfg = deepcopy(self.config)
        for section_name in cfg.sections():
            section = cfg[section_name]

            for key, value in section.items():
                if not value:
                    section.pop(key)

        with open(self.file_path, "w") as configfile:
            cfg.write(configfile)

    def _load_from_file(self) -> ConfigParser:
        config = ConfigParser()
        for section in self._SECTIONS_TO_KEYS.keys():
            config.add_section(section)

        if self.file_path.exists() and self.file_path.is_file():
            config.read(self.file_path)

        return config

    # try to load a setting, with this order of priorities:
    # 1. get it from an environment variable
    # 2. get it from the configparser
    # 3. use a default value (fallback)
    def _from_env_or_config(self, section: str, key: str, env_key: str, fallback_value=None):
        if env_key is not None:
            env_val = os.getenv(env_key)
            if env_val is not None:
                return env_val

        value = self.config.get(section, key, fallback=None)
        # We take advantage of "" being falsy
        if value:
            return value
        return fallback_value


__settings: Settings = Settings()


# Simplified, we don't need much more.
def get_settings() -> Settings:
    return __settings
