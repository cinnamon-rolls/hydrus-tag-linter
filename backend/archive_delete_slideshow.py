from backend.query import TagsQuery
from backend.query_slideshow import QuerySlideshow
from backend.hydrus import HydrusSearchRunner
from backend.actions import COMMON_ACTIONS


class ArchiveDeleteSlideshow(QuerySlideshow):
    def __init__(
            self,
            title : str = "Archive/Delete Filter",
            description : str = "A special slideshow to facilitate rapidly archiving or deleting items in your inbox"):
        super().__init__(TagsQuery(["system:inbox"]), title, description)

    def get_actions(self):
        import backend.actions as a
        return [
            a.ActionOverwriteShouldAdvance(a.ActionOverwriteShortcut(a.MoveToArchiveAction(), 'a'), advance_on_success = True),
            a.MoveToInboxAction(),
            a.ActionOverwriteShouldAdvance(a.ActionOverwriteShortcut(a.MoveToTrashAction(), 'd'), advance_on_success = True),
            a.MoveOutOfTrashAction(),
        ] + COMMON_ACTIONS

