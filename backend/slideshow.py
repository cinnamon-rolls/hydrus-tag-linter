from backend.hydrus import HydrusClient, HydrusFile, HydrusSearchRunner


class Slideshow:
    def __init__(self):
        self._hydrus_file_ids = None
        self._hydrus_metadata_cache = None
        self._position = 0

    def get_title(self):
        raise NotImplementedError()

    def get_description(self):
        raise NotImplementedError()

    def get_actions(self):
        raise NotImplementedError()

    # subclass should override this and return a list of Hydrus file IDs
    def _subclass_fetch_results(self, search_runner : HydrusSearchRunner) -> list[int]:
        raise NotImplementedError()

    def fetch_results(self, search_runner : HydrusSearchRunner):
        self._hydrus_file_ids = self._subclass_fetch_results(search_runner)
        self._hydrus_metadata_cache = [None] * len(self._hydrus_file_ids)
        self._position = 0

    @property
    def are_results_fetched(self):
        return self._hydrus_file_ids is not None

    def lazy_fetch_results(self, search_runner : HydrusSearchRunner):
        if not self.are_results_fetched:
            self.fetch_results(search_runner)

    @property
    def position(self):
        return self._position

    @property
    def size(self) -> int:
        return len(self._hydrus_file_ids)

    def __len__(self):
        return self.size

    @property
    def empty(self) -> bool:
        return self.size == 0

    def get_file_id(self, index: int) -> int:
        return self._hydrus_file_ids[index]

    def get_file_metadata(self, index: int, client: HydrusClient) -> dict:
        value = self._hydrus_metadata_cache[index]
        if value is not None:
            return value
        value = client.get_raw_file_metadata([self.get_file_id(index)])[0]
        self._hydrus_metadata_cache[index] = value
        return value

    def get_hash(self, index: int, client: HydrusClient) -> str:
        return self.get_file_metadata(index, client).get("hash")

    def get_hashes(self, client: HydrusClient, limit=None) -> list:
        if self.empty: return []

        if limit is None or limit >= self.size:
            limit = self.size

        # this is really really slow
        ret = []
        for index in range(0, limit):
            ret.append(self.get_hash(index, client))

        return ret

    def get_hydrus_file(self, index: int, client: HydrusClient) -> HydrusFile:
        return HydrusFile(self.get_file_metadata(index, client))

    def get_current_file_metadata(self, client: HydrusClient) -> dict:
        return self.get_file_metadata(self.position, client)

    def get_current_file(self, client: HydrusClient) -> HydrusFile:
        return self.get_hydrus_file(self.position, client)

    def invalidate_cached_metadata(self, index: int) -> None:
        self._hydrus_metadata_cache[index] = None

    def invalidate_current_file_metadata(self):
        self.invalidate_cached_metadata(self.position)

    def move_to_previous_file(self) -> None:
        self.move_to(self._position - 1)

    def move_to_next_file(self) -> None:
        self.move_to(self._position + 1)

    def move_to_first_file(self) -> None:
        self.move_to(0)

    def move_to_last_file(self) -> None:
        self.move_to(self.size - 1)

    def move_to(self, idx: int):
        max_idx = self.size - 1
        if idx < 0:
            idx = max_idx
        elif idx > max_idx:
            idx = 0
        self._position = idx
