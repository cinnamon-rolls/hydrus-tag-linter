from backend.query import Query
from backend.slideshow import Slideshow
from backend.hydrus import HydrusClient, HydrusSearchRunner
from backend.actions import COMMON_ACTIONS


class EmptySlideshow(Slideshow):
    def __init__(
            self,
            title : str = None,
            description : str = None):
        super().__init__()
        self._title = title
        self._description = description

    def get_title(self):
        return self._title

    def get_description(self):
        return self._description

    def get_actions(self):
        return []

    def _subclass_fetch_results(self, search_runner : HydrusSearchRunner) -> list[int]:
        return []

