from typing import Any

TYPE_NAMES = {
    int:   "Integer",
    float: "Float",
    str:   "String",
    bool:  "Boolean",
    list:  "List",
    dict:  "Dictionary",
    set:   "Set"
}


def check_type(value: Any, _type: type, value_name: str):
    if not isinstance(value, _type):
        value_type = type(value)
        expected_type_name = TYPE_NAMES.get(_type, _type.__name__)
        type_name = TYPE_NAMES.get(value_type, value_type.__name__)
        raise TypeError(f'Expected {value_name} to be of type "{expected_type_name}". Got "{type_name}" instead.')


def require_type(key, data: dict, _type: type, default=None, required: bool = False):
    value = data.get(key, default)
    if required and value is None:
        raise TypeError(f"A value of type '{_type.__name__}' for key '{key}' is expected.")
    if value != default:
        check_type(value, _type, value_name=f'"{key}"')
    return value


def require_list(key, data: dict, item_type: type = Any, default=None, required: bool = False) -> list:
    ret = require_type(key, data, list, default=default, required=required)
    if type(ret) is list and item_type != Any:
        for i in ret:
            check_type(i, item_type, value_name=f'element of list "{key}"')
    return ret


def require_int(key, data: dict, default=None, required: bool = False) -> int:
    return require_type(key, data, int, default=default, required=required)


def require_str(key, data: dict, default=None, required: bool = False, lower: bool = False, strip: bool = True) -> str:
    string = require_type(key, data, str, default=default, required=required)
    if type(string) is str:
        if lower:
            string = string.lower()
        if strip:
            string = string.strip()
    return string


def require_bool(key, data: dict, default=False, required: bool = False) -> bool:
    return require_type(key, data, bool, default=default, required=required)
