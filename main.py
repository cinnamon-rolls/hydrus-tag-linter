import backend.settings
import backend.hydrus
from ui.dialogs.dialog import ErrorConnectingDialog
from ui.dialogs.tag_services_dialog import TagServicesDialog
import ui.icons
import ui.main_window
import os
import sys
import traceback
import qtpy
from ui.dialogs import ErrorDialog, HydrusConnectDialog
from qtpy.QtCore import Qt
from qtpy.QtWidgets import QApplication, QMessageBox


def main():
    try:
        settings = backend.settings.get_settings()

        qt_api_version = qtpy.PYSIDE_VERSION if qtpy.PYSIDE_VERSION else qtpy.PYQT_VERSION
        print(f"QtPy: Qt {qtpy.QT_VERSION}", f"with {qtpy.API_NAME} ({qt_api_version})")

        QApplication.setAttribute(Qt.ApplicationAttribute.AA_ShareOpenGLContexts)
        app = QApplication(sys.argv)

        ui.icons.load_icons()

        style_sheet_path = settings.style_sheet_path
        if style_sheet_path is not None:
            if os.path.exists(style_sheet_path):
                with open(style_sheet_path) as f:
                    app.setStyleSheet(f.read())

        if settings.hydrus_auto_reconnect:
            # First time running the program, or the config got reset.
            if settings.hydrus_access_key is None:
                HydrusConnectDialog().exec()

            try:
                backend.hydrus.client().lazy_connect()
            except backend.hydrus.HydrusConnectionError as e:
                ErrorConnectingDialog(e).exec()

        if not settings.main_tag_service:
            TagServicesDialog().exec()

        ui.main_window.create_main_window()

        sys.exit(app.exec_())

    except Exception as e:
        traceback.print_exception(e)
        ErrorDialog("FATAL ERROR", str(e)).exec()
        sys.exit(1)


if __name__ == "__main__":
    main()
